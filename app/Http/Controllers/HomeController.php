<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index() {
        $proyectos = DB::table('proyectos')
        ->select('proyectos.*')
        ->get();

        $productos = DB::table('productos')
        ->select('productos.*')
        ->get();
        
    $banners = DB::table('banners')
        ->select('banners.*')
        ->get();
        return view('home', compact('productos', 'banners','proyectos'));       
    }
}
