<?php

namespace App\Http\Controllers;
use App\Models\Producto;
use App\Models\Carritocompra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
 
class ProductosController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    $productos = DB::table('productos')
        ->select('productos.*')
        ->get();
         //dd( $productos);
        return view ('productos')-> with('productos',$productos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Producto $id,Request $request)
    {   
        $carritocompra=new Carritocompra();

        $carritocompra->idCliente=$request->idCliente;
        $carritocompra->idProducto =$request->idProducto ;
        $carritocompra->total =$request->resultInput ;
        $carritocompra->ancho =$request->num1 ;
        $carritocompra->altura =$request->num2 ;
        $carritocompra->detalles =$request->descripcion;

        

        
        $carritocompra->save();
        $producto = Producto::find($id);
        return view ('comprarproducto',compact('producto'))->with('info', 'Se Agrego al carrito Correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $id)
    {
        $producto = Producto::find($id);
        //dd( $producto);
        return view ('comprarproducto',compact('producto'));
    }

       
      
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $id)
    {
    
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( )
    {
      
    }
    public function sum(Request $request) {
                
        $num1 = $request->input('num1');
        $num2 = $request->input('num2');
        $num3 = $request->input('num3');
      
        $sum = $num1 * $num2;
        $result = $sum * $num3;
        return response()->json(['result' => $result]);
    }
}
