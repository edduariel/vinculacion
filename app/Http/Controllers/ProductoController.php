<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;


class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = DB::table('productos')
                    ->select('productos.*')
                    ->get();
        return view('backend/productosBackend')-> with('productos',$productos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $productos=new Producto();

        $productos->producto=$request->producto;
        $productos->descripcion=$request->descripcion;
        $productos->precio=$request->precio;
        if ($request->hasfile('foto')) {
            $productos['foto'] = $request->file('foto')->store('uploads','public'); 
        }   
        $productos->save();
        return redirect()->to('/backend/homebackend/productos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $id)
    {
        $productos = Producto::find($id);
        //dd( $producto);
        return view ('/backend/productoseditar',compact('productos'));
  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $productos,$id)
    {
        $productos = Producto::find($id);
        $productos->producto=$request->producto;
        $productos->descripcion=$request->descripcion;
        $productos->precio=$request->precio;
        if ($request->hasfile('foto')) {
            Storage::delete('public/'.$productos->foto);
            $productos['foto'] = $request->file('foto')->store('uploads','public'); 
        }
        if ($request->hasfile('foto2')) {
            Storage::delete('public/'.$productos->foto2);
            $productos['foto2'] = $request->file('foto2')->store('uploads','public'); 
        }
        if ($request->hasfile('foto3')) {
            Storage::delete('public/'.$productos->foto3);
            $productos['foto3'] = $request->file('foto3')->store('uploads','public'); 
        }
        $productos->save();
        return redirect()->to('/backend/homebackend/productos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy( Producto $productos,$id)
    {
        Storage::delete('public/'.$productos->foto);
        $producto = Producto::find($id);
        $producto->delete();
        return redirect()->to('/backend/homebackend/productos');
    }
}
