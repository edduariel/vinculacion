<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\User;
use App\Models\Carritocompra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CarritoCompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carritocompras = DB::table('carritocompras')
        ->select('carritocompras.id','users.nombre','carritocompras.idCliente','productos.producto','productos.precio','productos.foto'
        ,'productos.descripcion','carritocompras.altura','carritocompras.ancho','carritocompras.total','carritocompras.detalles','carritocompras.numero_orden')
    
        ->join('productos', 'productos.id', '=', 'carritocompras.idProducto')
        ->join('users', 'users.id', '=', 'carritocompras.idCliente')

        ->get();


        
             
        return view('carritocompra', compact('carritocompras'));
    }

    public function indexCompra()
    {
        $carritocompras = DB::table('carritocompras')
    ->select('users.id','productos.id','users.nombre','carritocompras.idCliente','productos.producto','productos.precio','productos.foto'
    ,'productos.descripcion','carritocompras.altura','carritocompras.ancho','carritocompras.total','carritocompras.detalles','carritocompras.numero_orden')

    ->join('productos', 'productos.id', '=', 'carritocompras.idProducto')
    ->join('users', 'users.id', '=', 'carritocompras.idCliente')

    ->get();
   
    return view('compra', compact('carritocompras'));
    }
    public function updateCompra(Request $request){
        $idCliente = Auth::id();
        $numero_orden = "jf" . str_pad(rand(0, 99999999), 8, "0", STR_PAD_LEFT);

        $carritos = Carritocompra::where('idCliente', $idCliente)->whereNull('numero_orden')->get();
        foreach($carritos as $carrito) {
            $carrito->cedula=$request->cedula;
            $carrito->nombres=$request->nombres;
            $carrito->apellidos=$request->apellidos;
            $carrito->telefono=$request->telefono;
            $carrito->email=$request->email;
            $carrito->ciudad=$request->ciudad;
            $carrito->direccion=$request->direccion;
            $carrito->referencia=$request->referencia;
            $carrito->deposito=$request->deposito;
            $carrito->convencional=$request->convencional;
            $carrito->numero_orden =$numero_orden;
            if ($request->hasfile('deposito')) {
                Storage::delete('public/'.$carrito->deposito);
                $carrito['deposito'] = $request->file('deposito')->store('uploads','public'); 
            }
            $carrito->save();
        }
        
        return redirect()->back()->with('message', 'Los números de orden han sido actualizados con éxito');
    }

   public  function comprar() 
   {
    $carritocompras = DB::table('carritocompras')
    ->select('users.id','productos.id','users.nombre','carritocompras.idCliente','productos.producto','productos.precio','productos.foto'
    ,'productos.descripcion','carritocompras.altura','carritocompras.ancho','carritocompras.total','carritocompras.detalles')

    ->join('productos', 'productos.id', '=', 'carritocompras.idProducto')
    ->join('users', 'users.id', '=', 'carritocompras.idCliente')

    ->get();
   
    return view('compra', compact('carritocompras'));
   } 

   public function factura() 
   {
    return view('factura');
   }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carritocompra $carro, $id)
    {
        $carro = Carritocompra::find($id);
        $carro->delete();
        return redirect()->to('/carritocompra');
    }


}

