<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proyecto;
use Illuminate\Support\Facades\Storage;

class ProyectosBackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend/proyectos/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Proyecto $proyecto)
    {
        $proyecto=new Proyecto();
        $proyecto->nombre=$request->nombre;
        $proyecto->descripcion=$request->descripcion;
        if ($request->hasfile('foto')) {
            $proyecto['foto'] = $request->file('foto')->store('uploads','public'); 
        }   
        if ($request->hasfile('foto2')) {
            $proyecto['foto2'] = $request->file('foto2')->store('uploads','public'); 
        }  
        if ($request->hasfile('foto3')) {
            $proyecto['foto3'] = $request->file('foto3')->store('uploads','public'); 
        }
        $proyecto->save();
        return redirect()->to('backend/homebackend/proyectos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( Proyecto $proyecto)
    {
        return view('backend/proyectos/edit',compact('proyecto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proyecto $proyecto)
    { 
        $proyecto->nombre=$request->nombre;
        $proyecto->descripcion=$request->descripcion; 
        if ($request->hasfile('foto')) {
            Storage::delete('public/'.$proyecto->foto);
            $proyecto['foto'] = $request->file('foto')->store('uploads','public'); 
        }
        if ($request->hasfile('foto2')) {
            Storage::delete('public/'.$proyecto->foto2);
            $proyecto['foto2'] = $request->file('foto2')->store('uploads','public'); 
        }
        if ($request->hasfile('foto3')) {
            Storage::delete('public/'.$proyecto->foto3);
            $proyecto['foto3'] = $request->file('foto3')->store('uploads','public'); 
        }
        $proyecto->save();  
        return redirect()->to('backend/homebackend/proyectos');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proyecto $proyecto)
    {
        Storage::delete('public/'.$proyecto->foto,$proyecto->foto2,$proyecto->foto3);
        $proyecto->delete();
        return redirect()->to('backend/homebackend/proyectos');
    }
}

