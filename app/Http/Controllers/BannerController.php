<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend/banner/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Banner $banners)
    {
        $banners=new Banner();
        if ($request->hasfile('foto')) {
            $banners['foto'] = $request->file('foto')->store('uploads','public'); 
        }     
        $banners->save();
        return redirect()->to('backend/homebackend/banners');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( Banner $banner)
    {
        return view('backend/banner/edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    { 
        if ($request->hasfile('foto')) {
            Storage::delete('public/'.$banner->foto);
            $banner['foto'] = $request->file('foto')->store('uploads','public'); 
        }
        $banner->save();  
        return redirect()->to('backend/homebackend/banners');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        Storage::delete('public/'.$banner->foto);
        $banner->delete();
        return redirect()->to('backend/homebackend/banners');
    }
}
