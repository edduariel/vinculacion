<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Carritocompra;
use App\Models\Producto;
use Illuminate\Support\Facades\Storage;
class Ventas2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carritos = DB::table('carritocompras')
            ->select(
                'users.id',
                'users.nombre',
                'users.email',
                'users.telefono',
                'carritocompras.idCliente',
                'carritocompras.numero_orden',
                'carritocompras.nombres',
                'carritocompras.apellidos'
            )
            ->selectRaw('count(carritocompras.numero_orden)')
            ->join('users', 'users.id', '=', 'carritocompras.idCliente')
            ->whereNotNull('carritocompras.numero_orden')
            ->GROUPBY(
                'users.id',
                'carritocompras.numero_orden',
                'users.email',
                'users.telefono',
                'users.nombre',
                'carritocompras.idCliente',
                'carritocompras.nombres',
                'carritocompras.apellidos'
            )
            ->get();
        //dd( $productos);
        return view('backend/ventaslista', compact('carritos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $numero_orden, Producto $producto)
    {

        $carritos = Carritocompra::where('numero_orden', $numero_orden)
        ->join('productos', 'productos.id', '=', 'carritocompras.idProducto')
        ->select('carritocompras.*', 'productos.producto', 'productos.descripcion','productos.precio')
        ->get();
        
        return view('backend/ventashow', compact('carritos'));
    }

    public function showcliente($id, $numero_orden, Producto $producto)
    {

        $carritos = Carritocompra::where('numero_orden', $numero_orden)
        ->join('productos', 'productos.id', '=', 'carritocompras.idProducto')
        ->select('carritocompras.*', 'productos.producto', 'productos.descripcion','productos.precio')
        ->get();
        
        return view('backend/ventashow', compact('carritos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Carritocompra $carrito,$id)
    {
        Storage::delete('public/'.$carrito->deposito);
        $carrito = Carritocompra::find($id);
        $carrito->delete();
        return redirect()->to('/backend/homebackend/ventas');
    }
}
