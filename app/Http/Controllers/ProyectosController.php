<?php

namespace App\Http\Controllers;
use App\Models\Proyecto;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProyectosController extends Controller
{
    public function index() {
        $proyectos = DB::table('proyectos')
        ->select('proyectos.*')
        ->get();
         //dd( $productos);
        return view ('proyectos')-> with('proyectos',$proyectos);
    }
    

    public function indexP() {
        return view ('vistaproyecto');
    }
    public function show(Proyecto $id)
    {
        $proyecto = Proyecto::find($id);
        return view ('vistaproyecto',compact('proyecto'));
    }
}

