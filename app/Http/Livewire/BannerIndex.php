<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Banner;
use Livewire\WithPagination;

class BannerIndex extends Component
{
    use WithPagination;
    protected $paginationTheme='bootstrap';
    public function render()
    {        
        $banners= Banner::paginate();
        return view('livewire.banner-index',compact('banners'));
    }
}
