<?php

namespace App\Http\Livewire;

use App\Models\Model_has_role;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;


class UsersIndex extends Component
{
    use WithPagination;
    public $search;
    protected $paginationTheme='bootstrap';
    public function updatingSearch(){
        $this->resetPage();
    }
    public function render()
    {
            $users = Model_has_role::Join('users', 'users.id', '=', 'model_id')
            ->orderBy('id', 'ASC')
            ->get();

        return view('livewire.users-index', compact('users'));
    }
}
