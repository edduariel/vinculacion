<?php

namespace App\Http\Livewire;
use App\Models\Proyecto;
use Livewire\Component;
use Livewire\WithPagination;
class ProyectosIndex extends Component
{
    use WithPagination;
    public $search;
    protected $paginationTheme='bootstrap';
    public function updatingSearch(){
    $this->resetPage();
    }

    public function render()
    {
        $proyectos = Proyecto::where('nombre', 'like', '%'.$this->search.'%')->paginate();
        return view('livewire.proyectos-index', compact('proyectos'));
    }
}
