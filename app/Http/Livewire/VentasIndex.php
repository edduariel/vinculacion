<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Carritocompra;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;

class VentasIndex extends Component
{
    use WithPagination;
    public $search;
    protected $paginationTheme='bootstrap';
    public function render()
    {
        $carritos = DB::table('carritocompras')
            ->select('users.id','users.nombre','users.email','users.telefono','carritocompras.idCliente'
            ,'carritocompras.numero_orden','carritocompras.nombres','carritocompras.apellidos')
           ->selectRaw('count(carritocompras.numero_orden)')
            ->join('users', 'users.id', '=', 'carritocompras.idCliente')
            ->GROUPBY('users.id','carritocompras.numero_orden','users.email','users.telefono','users.nombre','carritocompras.idCliente'
            ,'carritocompras.nombres','carritocompras.apellidos')
            ->get();
        //dd( $productos);
        return view('livewire.ventas-index', compact('carritos'));
    }
}
