@include('header')
<header>
    <link rel="stylesheet" href="{{ asset('css/Registro.css')}}">
</header>

<body>
    <main>
        <div class="contendor-inf">
            <div>
                <h2>Titulo</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis hic nam nihil repudiandae ab, molestiae repellendus aut accusamus odit officia at totam. Consequuntur labore atque repudiandae ad libero odit expedita!</p>
                <br>
            </div>
            <div class="contenedor-informativo">
                <h3>
                    <img src="{{ asset('images/correo.png')}}"> &nbsp; Send Us an Email
                </h3>
                <p>
                    Descripcion
                </p>
                <p>
                    asdasd
                </p>
                <br>
            </div>
            <div class="contenedor-informativo">
                <h3>
                    <img src="{{ asset('images/llamada-telefonica.png')}}"> &nbsp; Send Us an Email
                </h3>
                <p>
                    Descripcion
                </p>
                <p>
                    asdasd
                </p>
                <br>
            </div>

        </div>
        <div class="contenedor-form">
            @csrf
            <div>
                <h2>
                    REGISTRO DE USUARIO
                </h2>
            </div>
            <form action="{{route('registro.store')}}" method="POST">
                @csrf
                <div class="contenedor-datosnb">
                    <div class="contenedor-dato2">
                        <img src="{{ asset('images/usuarioN.png')}}">
                        <input type="text" name="nombre" placeholder="Ingrese su nombre" required>
                    </div>
                    <div class="contenedor-dato3">
                        <input type="text" name="apellido" placeholder="Ingrese su apellido" required>
                    </div>
                </div>
                <div class="contenedor-dato">
                    <img src="{{ asset('images/CorreoN.png')}}">
                    <input type="text" name="email" placeholder="Ingrese su correo" required>
                </div>
                <div class="contenedor-dato">
                    <img src="{{ asset('images/Tel.png')}}">

                    <input type="int" name="telefono" placeholder="Ingrese su telefono" required>
                </div>
                <div class="contenedor-dato">
                    <img src="{{ asset('images/Contra.png')}}">
                    <input type="password" name="contraseña" placeholder="Ingrese su Contraseña" required>
                </div>
                <div class="contenedor-button">
                    <button type="submit" class="default-btn">Registrarse Ahora</button>
                    <br>
                </div>
            </form>
        </div>
    </main>
    @include('footer')
</body>