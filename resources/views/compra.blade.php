@include ('header')

<head>
    <link rel="stylesheet" href="{{ asset('css/CarritoCompra.css') }}">

</head>
<section class="Carrito">
    @auth
        @php
            $acumulador = 0;
            $contador = 0;
        @endphp
        @foreach ($carritocompras as $carrito)
            @if (auth()->user()->id == $carrito->idCliente && is_null($carrito->numero_orden))
                @php
                    $total = $carrito->total;
                    $acumulador += $total;
                @endphp
            @endif
        @endforeach

        <section>
            <article>
                <form action="{{ route('compra.updateCompra') }}" method="POST" class="formulariodatos" enctype="multipart/form-data">
                    @csrf
                    <h1>Información de la Compra</h1>
                    <div class="datosenvio">
                        <div class="datos">
                            <div>
                                <p>Cédula:</p>
                                <input type="text" class='form-control' name="cedula" id="cedula" required>
                            </div>
                            <div>
                                <p>Nombres:</p>
                                <input type="text" class='form-control' name="nombres" id="nombres" required>
                            </div>
                            <div>
                                <p>Apellidos:</p>
                                <input type="text" class='form-control' name="apellidos" id="apellidos" required> 
                            </div>
                        </div>
                        <div class="datos">
                            <div>
                                <p>Teléfono:</p>
                                <input type="text" class='form-control' name="telefono" id="telefono" required>
                            </div>
                            <div>
                                <p>Convencional:</p>
                                <input type="text" class='form-control' name="convencional" id="convencional" required>
                            </div>
                            <div>
                                <p>E-mail:</p>
                                <input type="text" class='form-control' name="email" id="email" required>
                            </div>
                        </div>
                    </div>

                    <!--Aqui van 3-->
                    <div class="direccion">
                        <div>
                            <p>Ciudad:</p>
                            <input type="text" class='form-control' name="ciudad" id="ciudad" required>
                        </div>
                        <div>
                            <p>Dirección:</p>
                            <input type="text" class='form-control' name="direccion" id="direccion" required>
                        </div>
                        <div>
                            <p>Referencia:</p>
                            <textarea class='form-control' name="referencia" id="referencia" required ></textarea>
                        </div>
                        <div>
                            <p>Comprobande de pago:</p>
                            <input type="file" class='form-control' name="deposito" id="deposito" required>
                        </div>
                    </div>
                    <div class="total">
                        <div class="total2">
                            <div class="espaciadototal">
                                <div class="izquierda">
                                    <h2 class="tex_p">Subtotal: </h2>
                                </div>
                                <div class="derecha">
                                    <h2>${{ $acumulador }}</h2>
                                </div>
                            </div>

                            <div class="espaciadototal">
                                <div class="izquierda">
                                    <h1 class="tex_p">Total a pagar: </h1>
                                </div>
                                <div class="derecha">
                                    <h1>${{ $acumulador }}</h1>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="botonenviar">
                    


                        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"
                        data-whatever="@mdo"> <h3>COMPRAR</h3></button>
                    </div>

                </form>
              
            </article>
        </section>


    @endauth
    @guest
        <div class="Carrito_producto">
            <h1>Inicie Sesion Primero</h1>
        </div>
    @endguest


</section>




@include ('footer')
