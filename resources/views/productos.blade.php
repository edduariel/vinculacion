@include ('header')

<body style="background-color:rgba(0,0,0,.1) ;">


  <article class="Titulo-Productos">
    <h1 class=""> Nuestros <span>Productos</span> </h1>
  </article>
  <br>
<main>



  @foreach ($productos as $productomostrado)
  <div class="contodo">
    <form action="{{route('producto.show', $productomostrado->id)}} " method="post" class="formproducto" >

      <button type="submit" data-id="{{ $productomostrado->id}}" class="Productos-Mostrados">
        <div class="Productos-Mostrados" data-name="p-1">
          <div class="Imagen">
            <img src="{{asset('storage').'/'.$productomostrado->foto}}">
          </div>
          <div class="Contenido-producto">
            <div>
            <h3>{{$productomostrado->producto  }} </h3>
            </div>
            <div class="descripcionproducto">
              <h4>{{$productomostrado->descripcion}}</h4>
            </div>
            @csrf
            @method('post')
            <div class="">
              <input type="hidden" name="id" value="{{$productomostrado->id}}">
              <input type="hidden" namespace="_method" value="show">
            </div>
          </div>
        </div>
      </button>






    </form>
  </div>

  @endforeach

  </main>





  <!-- footer section ends -->


  <!-- custom js file link  -->


</body>

</html>