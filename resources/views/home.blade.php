@include ('header')
<header>
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
    <link rel="stylesheet" href="{{ asset('css/proyectos.css') }}">
</header>
<main>
    <div class="margenb">
        <div class="margenbanner">
            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel" style="position:static">
                <div class="carousel-inner" style="position:static">
                    @foreach ($banners as $bannersmostrado)
                        <div class="carousel-item active" style="position:static">
                            <div class="imagenbanner">
                                <img src="{{ asset('storage') . '/' . $bannersmostrado->foto }}" class="d-block w-100">

                            </div>
                        </div>
                    @endforeach
                    <div class="previmg">
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                            data-bs-slide="prev" style="position:static">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                    </div>

                    <div class="nextimg">
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                            data-bs-slide="next" style="position:static">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contenedorinformacion1">
        <h1>¡Bienvenidos a nuestra agencia de publicidad!</h1>
        <hr>
    </div>
    <div class="quienesomos">
        <div class="contenimosqs">
            <h2>Quienes somos Nosostros</h2>
            <p>Si estás buscando una empresa creativa y confiable para llevar a cabo tus campañas publicitarias, ¡has
                llegado al lugar correcto!

                Somos un equipo de profesionales altamente cualificados y estamos comprometidos a ayudar a nuestros
                clientes a alcanzar sus objetivos de marketing. Ofrecemos una amplia gama de servicios, desde diseño
                gráfico hasta campañas publicitarias en redes sociales.

                ¿Quieres saber más sobre nosotros? ¡No dudes en explorar nuestro sitio web y conocer nuestro trabajo! Si
                tienes alguna pregunta o quieres contratar nuestros servicios, no dudes en ponerte en contacto con
                nosotros. ¡Estaremos encantados de ayudarte!
                Qué ofrece nuestro servicio</p>
        </div>
    </div>

    <div class="titulogeneralhome2">
        <h4>Nuestros Productos</h4>
    </div>
    <div class="descripciongeneralhome">
        <p>Nuestros productos incluyen el Diseño grafico y entregas a Nivel de Manta</p>
    </div>
    <hr>
    <br>
    <br>



    <div class="margen3">
        <br>
        <br>
        <br><br>
        <div class="generalproyect3">

            @foreach ($productos->shuffle()->slice(0, 3) as $productomostrado)
                <form action="{{ route('producto.show', $productomostrado->id) }} " method="post">

                    <div class="espaciado3">
                        <div class="tarjetas3">
                            <img src="{{ asset('storage') . '/' . $productomostrado->foto }}" class="imgproyecto3">
                            <div class="Titulopryecto3">
                                <h3 class="tituloproyect3">{{ $productomostrado->producto }} </h3>
                            </div>
                            <div class="descripcionproyecto3">
                                <div class="textop3">
                                    <p class="descripcionp3">{{ $productomostrado->descripcion }}</p>
                                </div>
                            </div>
                            <button class="verproyecto3" type="submit" data-id="{{ $productomostrado->id }}">
                                <h4>Ver Producto</h4>
                                @csrf
                                @method('post')
                                <div class="">
                                    <input type="hidden" name="id" value="{{ $productomostrado->id }}">
                                    <input type="hidden" namespace="_method" value="show">
                                </div>
                            </button>
                        </div>

                    </div>
                </form>
            @endforeach

        </div>

    </div>
    <br>
    <br>
    <br>
    <div class="titulogeneralhome2">
        <h4>Nuestros Proyectos</h4>
    </div>
    <div class="descripciongeneralhome">
        <p>Cada uno de nuestros proyectos requiere la combinación de habilidades en diseño gráfico, ingeniería, mecánica
            y logística para llevar a cabo una instalación exitosa.
            Gracias a esto nos encargamos de crear y ejecutar estrategias de promoción y marketing a través de diversos
            medios publicitarios en distintos espacios y ambientes. Algunos ejemplos de proyectos que podría llevar a
            cabo son:</p>
    </div>
    <hr>
    <br>
    <br>
    <div class="margen2">
        <br>
        <br>
        <br><br>
        <div class="generalproyect2">

            @foreach ($proyectos->shuffle()->slice(0, 3) as $proyecto)
                <form action="{{ route('proyectos.show', $proyecto->id) }} " method="post">

                    <div class="espaciado2">
                        <div class="tarjetas2">
                            <img src="{{ asset('storage') . '/' . $proyecto->foto }}" class="imgproyecto2">
                            <div class="Titulopryecto2">
                                <h3 class="tituloproyect2">{{ $proyecto->nombre }} </h3>
                            </div>
                            <div class="descripcionproyecto2">
                                <div class="textop2">
                                    <p class="descripcionp2">{{ $proyecto->descripcion }}</p>
                                </div>
                            </div>
                            <button class="verproyecto2" type="submit" data-id="{{ $proyecto->id }}">
                                <h4>Leer Más</h4>
                                @csrf
                                @method('post')
                                <div class="">
                                    <input type="hidden" name="id" value="{{ $proyecto->id }}">
                                    <input type="hidden" namespace="_method" value="show">
                                </div>
                            </button>
                        </div>

                    </div>
                </form>
            @endforeach

        </div>

    </div>


    </div>
    <br><br>
</main>
@include ('footer')
</body>
