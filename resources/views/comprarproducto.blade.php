<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Publicidad J.S</title>
    <link rel="stylesheet" href="{{ asset('css/Encabezado.css') }}">

    <link rel="stylesheet" href="{{ asset('css/Comprarproducto.css') }}">
    <link rel="stylesheet" href="{{ asset('css/Encabezado.css') }}">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous">
    </script>
</head>
<header>
    <input type="checkbox" id="btn-menulateral" style="display:none ;">
    <!--No mover de aqui o si no dejara de funcionar el menú lateral-->
    <div class="MenuLateral">
        <div class="icono-cerra-menu">
            <label for="btn-menulateral">x</label>
        </div>
        <div class="Contenido-Menu">
            <div class="Menu-Logo">
                <img src="{{ asset('images/jfpublicidadlog.png') }}">
                <p>Publicidad Jorge Flores</p>
            </div>
            <nav class="Menu-Navegación">
                <a href="">Inicio</a>
                <a href="">Producto</a>
                <a href="">Servicio</a>
                <a href="">FQA</a>
            </nav>
        </div>
    </div>
    <div class="Encabezado">
        <div class="contenedorlogo">
            <img src="{{ asset('images/jfpublicidadlog.png') }}">
        </div>
        <nav class="contenedornavegacion">
            <a href="{{ asset('home') }}">Inicio</a>
            <a href="{{ asset('productos') }}">Productos</a>
            <a href="{{ asset('proyectos') }}">Proyectos</a>

        </nav>
        <nav class="conetenedorbotones">
            <nav class="contenedornavegacion">
                @auth
                    <a class="joselo">Bienvenido {{ auth()->user()->nombre }}</a>
                @endauth
            </nav>
            @auth
                <div>
                    <a class="default-btn" class="default-btn" href="{{ asset('carritocompra') }}"> Mis Compras
                        <span></span></a>
                </div>
            @endauth
            @guest
                <div>
                    <a class="default-btn" class="default-btn" href="{{ asset('login') }}"> Mis Compras <span></span></a>
                </div>
                <div>
                    @csrf
                    <a class="default-btn" href="{{ asset('login') }}">Login <span></span></a>
                </div>
                <div>
                    <a class="default-btn" href="{{ asset('registro') }}">Registro <span></span></a>
                </div>
            @endguest
            @auth
                <div>
                    <a class="default-btn" href="{{ route('login.logout') }}">Cerrar Sesion<span></span></a>
                </div>
            @endauth
            <div class="control-icono-menu">
                <label for="btn-menulateral"><img src="{{ asset('images/menu.png') }}"
                        class="control-icono-menu"></label>
            </div>
        </nav>
    </div>
    <script type="text/javascript">
        window.addEventListener("scroll", function() {
            var header = document.querySelector("header");
            header.classList.toggle("abajo", window.scrollY > 0);
        })
    </script>
</header>

<body>

   <br>
   <br>
   
    <main>

        <div class="contenidocomprar">

            @foreach ($producto as $productomostrado)
                <div>
                    <form action="{{ route('producto.store', $productomostrado->id) }} " method="post"
                        class="formproducto">
                        @csrf
                        @method('post')

                        @auth
                            @if (session('info'))
                                <div class="alert alert-success">
                                    <strong>{{ session('info') }}</strong>
                                </div>
                            @endif

                            <div class="ProductosEstructura" data-name="p-1">

                                <div class="Contenido-productocomprar">
                                    <div class="tituloproductocomprarocultar">
                                        <h3>{{ $productomostrado->producto }} </h3>
                                    </div>

                                    <div class="ImagenComprar">
                                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel"
                                            class="pruebitauwu">
                                            <div class="carousel-inner" id="prubechucha">
                                                <div class="carousel-item active">
                                                    <img src="{{ asset('storage') . '/' . $productomostrado->foto }}"
                                                        class="imagenslider" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{ asset('storage') . '/' . $productomostrado->foto2 }}"
                                                        class="imagenslider" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{ asset('storage') . '/' . $productomostrado->foto3 }}"
                                                        class="imagenslider" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button"
                                                data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                                            </button>
                                            <button class="carousel-control-next" type="button"
                                                data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>

                                            </button>
                                        </div>
                                    </div>
                                    <div class="precioproductocomprarocultar">
                                        <h3>$ {{ $productomostrado->precio }} </h3>
                                    </div>
                                    <div class="Informacionproductocomprarocultar">
                                        <h4>Diseño Grafico Incluido</h4>
                                        <h4>Transporte Gratis a Nivel de Manabi</h4>
                                    </div>

                                    <div class="ocultar">


                                        <div class="tituloproductocomprar">
                                            <h3 class="nombreproducto">{{ $productomostrado->producto }} </h3>
                                        </div>
                                        <div class="precioproductocomprar">

                                            <h3 class="preciopp">
                                                $ {{ $productomostrado->precio }}  <p class="textprecio"> Precio por Metro Cuadrado </p>  </h3>
                                        </div>
                                        <div>
                                            <h4 class="descripcionproduct">{{ $productomostrado->descripcion }}</h4>
                                        </div>
                                        <div class="Informacionproductocomprar">
                                            <h4 class="disenografico">Diseño Grafico Incluido</h4>
                                            <h4 class="transportepro">Transporte Gratis en Manta</h4>
                                        </div>
                                        <p class="ingresodatos"> Ingrese los Datos en Metros para Calcular el Costo del Producto</p>
                                        <label>
                                            <h4 class="datosproduc"> Numero el Ancho:</h4>
                                        </label>
                                        <input class="inputtex" type="text" name="num1" id="num1" required>
                                        <br>
                                        <label>
                                            <h4 class="datosproduc"> Numero la Altura:</h4>
                                        </label>
                                        <input class="inputtex"  type="text" name="num2" id="num2" required>
                                        <br>
                                        <label>
                                            <h4 class="datosproduc"> Mas Detalles:</h4>
                                        </label>
                                        <input  class="inputtex"  type="text" name="descripcion" id="descripcion" required >
                                    
                                       <h1 class="preciototalreal" >
                                    
                                            Precio a Pagar: $
                                            
                                            <div class="posicionprecio" id="result">                                               
                                            </div>
                                       
                                        </h1> 

                                        <input type="hidden" id="num3" value="{{ $productomostrado->precio }} ">

                                        <div id="result">

                                        </div>
                                        <div class="botoncitocompraaar">
                                            <button type="submit" class="btn btn-primary">AGREGAR A CARRITO</button>
                                        </div>
                                        @csrf
                                        @method('post')
                                        <div class="">
                                            <input type="hidden" name="id" value="{{ $productomostrado->id }}">
                                            <input type="hidden" namespace="_method" value="show">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{ $productomostrado->id }}">
                            <input type="hidden" namespace="_method" value="show">
                            <input type="text" value="{{ $productomostrado->id }}" name="idProducto"
                                style="display:none">
                            <input type="text" value="{{ auth()->user()->id }}" name=" idCliente"
                                style="display:none">

                            <input type="hidden" id="resultInput" value="" name="resultInput">

                            <button type="submit" class="btn btn-primary" id="precioproductocomprarocultar">AGREGAR A
                                CARRITO</button>



                        </form>

                    @endauth
                    @guest
                        <div class="ProductosEstructura" data-name="p-1">

                            <div class="Contenido-productocomprar">
                                <div class="tituloproductocomprarocultar">
                                    <h3>{{ $productomostrado->producto }} </h3>
                                </div>

                                <div class="ImagenComprar">
                                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel"
                                        class="pruebitauwu">
                                        <div class="carousel-inner" id="prubechucha">
                                            <div class="carousel-item active">
                                                <img src="{{ asset('storage') . '/' . $productomostrado->foto }}"
                                                    class="imagenslider" alt="...">
                                            </div>
                                            <div class="carousel-item">
                                                <img src="{{ asset('storage') . '/' . $productomostrado->foto2 }}"
                                                    class="imagenslider" alt="...">
                                            </div>
                                            <div class="carousel-item">
                                                <img src="{{ asset('storage') . '/' . $productomostrado->foto3 }}"
                                                    class="imagenslider" alt="...">
                                            </div>
                                        </div>
                                        <button class="carousel-control-prev" type="button"
                                            data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Previous</span>
                                        </button>
                                        <button class="carousel-control-next" type="button"
                                            data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Next</span>
                                        </button>
                                    </div>
                                </div>
                                <div class="precioproductocomprarocultar">
                                    <h3>$ {{ $productomostrado->precio }} </h3>
                                </div>
                                <div class="Informacionproductocomprarocultar">
                                    <h4>Diseño Grafico Incluido</h4>
                                    <h4>Transporte Gratis a Nivel de Manabi</h4>
                                </div>

                                <div class="ocultar">


                                    <div class="tituloproductocomprar">
                                        <h3>{{ $productomostrado->producto }} </h3>
                                    </div>
                                    <div class="precioproductocomprar">
                                        <h3>$ {{ $productomostrado->precio }} </h3>
                                    </div>
                                    <div>
                                        <h4>{{ $productomostrado->descripcion }}</h4>
                                    </div>
                                    <div class="Informacionproductocomprar">
                                        <h4>Diseño Grafico Incluido</h4>
                                        <h4>Transporte Gratis a Nivel de Manabi</h4>
                                    </div>
                                    <form action="{{ asset('login') }}">
                                        <div class="botoncitocompraaar">
                                            <button type="submit" class="btn btn-primary">AGREGAR A CARRITO</button>
                                        </div>
                                    </form>

                                    @csrf
                                    @method('post')
                                    <div class="">
                                        <input type="hidden" name="id" value="{{ $productomostrado->id }}">
                                        <input type="hidden" namespace="_method" value="show">
                                    </div>
                                </div>
                            </div>
                        </div>



                    @endguest
                </div>
            @endforeach
        </div>
        <br>
        <br>
        <br>
        
        
        @include('footer')
    </main>



    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        $('#num1').keyup(function() {
            var num1 = $('input[name=num1]').val();
            var num2 = $('input[name=num2]').val();
            var num3 = $('input[name=num3]').val();
            $.ajax({
                type: 'GET',
                url: 'http://localhost/vinculacion/public/productos/calculo',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    num1: num1,
                    num2: num2,
                    num3: num3
                },
                success: function(data) {
                    console.log(data);
                    $('#resultInput').val(data.result);
                    $('#result').html(data.result);
                }
            });
        });

        $('#num1').keyup(function() {
            var num1 = $('#num1').val();
            var num2 = $('#num2').val();
            var num3 = $('#num3').val();

            $.ajax({
                type: 'GET',
                url: 'http://localhost/vinculacion/public/productos/calculo',
                data: {
                    num1: num1,
                    num2: num2,
                    num3: num3,
                },
                success: function(data) {
                    console.log(data);
                    $('#resultInput').val(data.result);
                    $('#result').html(data.result);
                }
            });
        });

        $('#num2').keyup(function() {
            var num1 = $('#num1').val();
            var num2 = $('#num2').val();
            var num3 = $('#num3').val();

            $.ajax({
                type: 'GET',
                url: 'http://localhost/vinculacion/public/productos/calculo',
                data: {
                    num1: num1,
                    num2: num2,
                    num3: num3,
                },
                success: function(data) {
                    console.log(data);
                    $('#resultInput').val(data.result);
                    $('#result').html(data.result);
                }
            });
        });

        $('#num3').keyup(function() {
            var num1 = $('#num1').val();
            var num2 = $('#num2').val();
            var num3 = $('#num3').val();

            $.ajax({
                type: 'GET',
                url: 'http://localhost/vinculacion/public/productos/calculo',
                data: {
                    num1: num1,
                    num2: num2,
                    num3: num3,
                },
                success: function(data) {
                    console.log(data);
                    $('#resultInput').val(data.result);
                    $('#result').html(data.result);
                }
            });
        });
    </script>



    <script>
        const carousel = new bootstrap.Carousel('#myCarousel')
    </script>








</body>

