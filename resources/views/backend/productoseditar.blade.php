@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Asignar un Rol</h1>
@stop

@section('content')
@if (session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
    
@endif


    
<div class="card">
    
    <div class="card-body">
    @foreach ($productos as $productomostrado)
   
  
    <form action="{{route('productosbackend.update',$productomostrado->id )}}" method="POST" enctype="multipart/form-data">
    @csrf
        <p class="h5">Nombre:</p>
        <input class="form-control" name="producto" value="{{$productomostrado->producto}}" required size="500" maxlength="500">
        <p class="h5">Descripcion:</p>
        <input class="form-control" name="descripcion" value="{{$productomostrado->descripcion}}" required size="1000" maxlength="1000">
        <p class="h5">Precio:</p>
        <input class="form-control" name="precio" value="{{$productomostrado->precio}}" required>
        <p class="h5">Foto:</p>
        <input type="file" class="form-control" id="recipient-name" name="foto" required>
        <img src="{{asset('storage').'/'.$productomostrado->foto}}" alt="" width="60" required>
        <input type="file" class="form-control" id="recipient-name" name="foto2">
        <img src="{{asset('storage').'/'.$productomostrado->foto2}}" alt="" width="60" required>
        <input type="file" class="form-control" id="recipient-name" name="foto3">
        <img src="{{asset('storage').'/'.$productomostrado->foto3}}" alt="" width="60" required>
      
        <br>
        <br>
        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Editar</button>
        </form>
       
    </div>
    @endforeach
</div>


@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop