@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Detalles de Pedido</h1>
@stop

@section('content')
    @php
        $prevNumeroOrden = null;
    @endphp
    @php
            $acumulador = 0;
            $contador = 0;
        @endphp
    @foreach ($carritos as $carrito)
        @if ($prevNumeroOrden != $carrito->numero_orden)
        
    <header>
        <h1>Detalles de Compra</h1>
    </header>
    <section>
        <body>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"
                data-whatever="@mdo">Ver Comprobante</button>
    
    
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nuevo Proyecto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                           <center>
                            <img src="{{ asset('storage') . '/' . $carrito->deposito }}" alt=""
                            width="450">
                            </center> 
                        </div>
    
                    </div>
                </div>
            </div>

            <article class="numerorden">
                <div>
                    <h2>Datos del cliente</h5>
                </div>
                <div>
                    <p>No.de Orden: {{ $carrito->numero_orden }}</p>
                </div>
            </article>
            <section class="datos">
                <article class="datosorden">
                    <div class="datosclientes">
                        <h5>Nombres:</h5>
                    </div>
                    <div class="datosclientes">
                        <span>{{ $carrito->nombres }}
                            {{ $carrito->apellidos }}</span>
                    </div>
                </article>
                <article class="datosorden">
                    <div class="datosclientes">
                        <h5>Cédula:</h5>
                    </div>
                    <div class="datosclientes">
                        <p>{{ $carrito->cedula }}</p>
                    </div>
                </article>
                <article class="datosorden">
                    <div class="datosclientes">
                        <h5>Email:</h5>
                    </div>
                    <div class="datosclientes">
                        <p>{{ $carrito->email }}</p>
                    </div>
                </article>
                <article class="datosorden">
                    <div class="datosclientes">
                        <h5>Telefono:</h5>
                    </div>
                    <div class="datosclientes">
                        <p>{{ $carrito->telefono }}</p>
                    </div>
                </article>
                <article class="datosorden">
                    <div class="datosclientes">
                        <h5>Convecional:</h5>
                    </div>
                    <div class="datosclientes">
                        <p>{{ $carrito->convencional }}</p>
                    </div>
                </article>
                <article class="datosorden">
                    <div class="datosclientes">
                        <h5>Ciudad:</h5>
                    </div>
                    <div class="datosclientes">
                        <p>{{ $carrito->ciudad }}</p>
                    </div>
                </article>
                <article class="datosorden">
                    <div class="datosclientes">
                        <h5>Direccion:</h5>
                    </div>
                    <div class="datosclientes">
                        <p>{{ $carrito->direccion }}</p>
                    </div>
                </article>
                <article class="datosorden">
                    <div class="datosclientes">
                        <h5>Referencia:</h5>
                    </div>
                    <div class="datosclientes">
                        <p>{{ $carrito->referencia }}</p>
                    </div>
                </article>
               
                    @php
                        $prevNumeroOrden = $carrito->numero_orden;
                    @endphp
                    <br>
        <br>
        <article>

            <h5>Detalles del pedido</h5>
        </article>
        <article>
            <table class="table table-striped">
                <thead>
                    <tr>

                        <th scope="col" class="alineado">Cantidad</th>
                        <th scope="col" class="alineado">Producto</th>
                        <th scope="col" class="alineado">Descripcion</th>
                        <th scope="col" class="alineado">Alto</th>
                        <th scope="col" class="alineado">Ancho</th>
                        <th scope="col" class="alineado">Precio</th>
                        <th scope="col" class="alineado">Total</th>
                    </tr>
                </thead>
        @endif
        @php
                            $total = $carrito->total;
                            $acumulador += $total;
                        @endphp
                <tbody>
                    <tr>
                        <th scope="row" class="alineado">1</th>
                        <td class="alineado">{{ $carrito->producto }}</td>
                        <td class="alineado">{{ $carrito->descripcion }}</td>
                        <td class="alineado">{{ $carrito->altura }}</td>
                        <td class="alineado">{{ $carrito->ancho }}</td>
                        <td class="alineado">{{ $carrito->precio }}</td>
                        <td class="alineado">{{ $carrito->total }}</td>


                    </tr>

                    </tr>
                    <!-- Aqui no va foreach pelaverga-->
    @endforeach
    <tr>
        <th scope="row" class="alineado"></th>
        <td class="alineado"></td>
        <td class="alineado"></td>
        <td class="alineado"></td>
        <td class="alineado"></td>
        <td class="alineado">Subtotal</td>
        <td class="alineado">{{ $acumulador }}</td>
    </tr>
    <tr>
        <th scope="row" class="alineado"></th>
        <td class="alineado"></td>
        <td class="alineado"></td>
        <td class="alineado"></td>
        <td class="alineado"></td>
        <td class="alineado">Iva</td>
        <td class="alineado">0</td>
    </tr>
    <tr>
        <th scope="row" class="alineado"></th>
        <td class="alineado"></td>
        <td class="alineado"></td>
        <td class="alineado"></td>
        <td class="alineado"></td>
        <td class="alineado">Total</td>
        <td class="alineado">{{ $acumulador }}</td>
    </tr>
    </tbody>
    </table>
    </article>
    </section>
    <br>
    <br>
    <div style="text-align: center;">
        <button type="button" class="btn btn-primary btn-lg">Imprimir </button>
    </div>

    </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('css/factura.css') }}">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
