@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar Proyecto</h1>
@stop

@section('content')
@if (session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
    
@endif
<form action="{{route('backend.proyectos.update',$proyecto )}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
<div class="card">
    <div class="card-body">
        <p class="h5">Nombre:</p>
        <input class="form-control" name="nombre" value="{{$proyecto->nombre}}" required size="200" maxlength="200">
        <p class="h5">Descripcion:</p>
        <input class="form-control" name=descripcion value="{{$proyecto->descripcion}}" required size="1000" maxlength="1000">
        <p class="h5">Imagen Principal:</p>
        <img src="{{asset('storage').'/'.$proyecto->foto}}" alt="" width="60" required>
        <input type="file" class="form-control" id="recipient-name" name="foto" required>
        <p class="h5">Imagen:</p>
        <img src="{{asset('storage').'/'.$proyecto->foto2}}" alt="" width="60">
        <input type="file" class="form-control" id="recipient-name" name="foto2" required>
        <p class="h5">Imagen:</p>
        <img src="{{asset('storage').'/'.$proyecto->foto3}}" alt="" width="60">
        <input type="file" class="form-control" id="recipient-name" name="foto3" required>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Actualizar</button>
    </div>
</div>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop