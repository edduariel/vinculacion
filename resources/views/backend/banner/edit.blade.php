@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar Proyecto</h1>
@stop

@section('content')
@if (session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
    
@endif
<form action="{{route('backend.banner.update',$banner )}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
<div class="card">
    <div class="card-body">
        <p class="h5">Imagen:</p>
        <img src="{{asset('storage').'/'.$banner->foto}}" alt="" width="60">
        <input type="file" class="form-control" id="recipient-name" name="foto" required>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Actualizar</button>
    </div>
</div>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop