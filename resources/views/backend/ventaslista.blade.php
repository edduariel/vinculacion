@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Lista de Ventas</h1>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <input wire:model="search" class="form-control" placeholder="Ingrese el Nombre o Correo de un Usuario">
    </div>
    @if ($carritos->count())

        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Numero de Orden</th>
                        <th>ID Cliente</th>
                        <th>Correo</th>
                        <th>Telefono</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    
                    @foreach ($carritos as $carrito)
                        <tr>
                            <td>{{ $carrito->numero_orden }}</td>
                            <td>{{ $carrito->idCliente }}</td>
                            <td>{{ $carrito->email }}</td>
                            <td>{{ $carrito->telefono }}</td>
                            <td>
                                <form action="{{ route('ventas.show', ['id' => $carrito->id, 'numero_orden' => $carrito->numero_orden]) }}" style="margin-right:10px;">
                                    <input type="hidden" namespace="_method" value="show">
                                    <button type="submit" class="btn btn-primary " data-id="{{ $carrito->id }}">
                                        Ver
                                    </button>
                                </form>
                            </td>

                            <td>
                                <form action="{{ route('ventas.destroy', $carrito->id)}}" method="post" id="formEli_{{ $carrito->id}}" >
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id" value="{{$carrito->id}}">
                                    <input type="hidden" namespace="_method" value="delete">
                                    <button type="submit" class="btn btn-primary btnModalEliminar" data-id="{{ $carrito->id}}"> 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                   <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                                    </svg>  
                                    </button>
                                    </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                </ul>
            </nav>
        </div>
    @else
        <div class="card-body">
            <strong>No hay Registros</strong>

        </div>


    @endif
</div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop