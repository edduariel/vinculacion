@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Asignar un Rol</h1>
@stop

@section('content')
@if (session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
    
@endif
<form action="{{route('backend.users.update',$user )}}" method="POST">
    @csrf
    @method('put')
<div class="card">
    <div class="card-body">
        <p class="h5">Nombre:</p>
        <input class="form-control" name="nombre" value="{{$user->nombre}}" required size="50" maxlength="50">
        <p class="h5">Apellido:</p>
        <input class="form-control" name="apellido" value="{{$user->apellido}}" required size="50" maxlength="50">
        <p class="h5">Email:</p>
        <input type="email" class="form-control" name="email" value="{{$user->email}}" required size="80" maxlength="80">
        <p class="h5">Telefono:</p>
        <input class="form-control" name="telefono" value="{{$user->telefono}}" required>
        <h2 class="h5">Listado de Roles</h2>
        {!! Form::model($user, ['route'=>['backend.users.update', $user],'method'=>'put']) !!}
            @foreach ($roles as $role)
                <div>
                    <label >
                        {!! Form::checkbox('roles[]', $role->id, null, ['class'=>'mr-1']) !!}
                        {{$role->name}}
                    </label>
                </div>           
            @endforeach
            {!! Form::submit('Editar', ['class'=>'btn btn-primary mt-2']) !!}
        {!! Form::close() !!}
    </div>
</div>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop