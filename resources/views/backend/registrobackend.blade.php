<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <main>
        <div class="home">
            <form action="{{route('registrobackend.store')}}" method="POST">
                @csrf
                <div class="contenedor_login">

                    <h2>REGISTRO DE USUARIO backend</h2>
                    <label>
                        <input type="email" name="email" placeholder="Ingrese su nombre" required size="80" maxlength="80">
                    </label>
                    <br>
                    <label>
                        <input type="password" name="contraseña" placeholder="Ingrese su Contraseña" required size="200" maxlength="200">
                    </label>
                    <br>
                    <label>
                        <input type="number" name="idRol" placeholder="Ingrese su rol" required>
                    </label>
                    <a class="btn btn-primary" type="submit">Registrarse Ahora</a>
                </div>
            </form>

        </div>
    </main>
</body>

</html>