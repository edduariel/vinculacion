<div class="card">
    <div class="card-header">
        <input wire:model="search" class="form-control" placeholder="Ingrese el Nombre o Correo de un Usuario">
    </div>
    @if ($carritos->count())

        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Numero de Orden</th>
                        <th>ID Cliente</th>
                        <th>Correo</th>
                        <th>Telefono</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($carritos as $carrito)
                        <tr>
                            <td>{{ $carrito->numero_orden }}</td>
                            <td>{{ $carrito->idCliente }}</td>
                            <td>{{ $carrito->email }}</td>
                            <td>{{ $carrito->telefono }}</td>
                            <td>
                                <form action="{{ route('backend.ventas.show', [$carrito->id, $carrito->numero_orden]) }}" style="margin-right:10px;">
                                    <input type="hidden" namespace="_method" value="show">
                                    <button type="submit" class="btn btn-primary " data-id="{{ $carrito->id }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                            fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                                            <path
                                                d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z" />
                                        </svg>
                                    </button>
                                </form>
                            </td>

                            <td>
                                <a class="btn btn-primary" href="">Eliminar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                </ul>
            </nav>
        </div>
    @else
        <div class="card-body">
            <strong>No hay Registros</strong>

        </div>


    @endif
</div>
