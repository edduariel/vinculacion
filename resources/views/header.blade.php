<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Publicidad J.S</title>
    <link rel="stylesheet" href="{{ asset('css/Encabezado.css')}}">
    <link rel="stylesheet" href="{{ asset('css/Productos.css')}}">
  
    <link rel="stylesheet" href="{{ asset('css/Proyectos.css')}}">
    <link rel="stylesheet" href="{{ asset('css/login.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</head>
<header>
    <input type="checkbox" id="btn-menulateral" style="display:none ;">
    <!--No mover de aqui o si no dejara de funcionar el menú lateral-->
    <div class="MenuLateral">
        <div class="icono-cerra-menu">
            <label for="btn-menulateral">x</label>
        </div>
        <div class="Contenido-Menu">
            <div class="Menu-Logo">
                <img src="{{ asset('images/jfpublicidadlog.png')}}">
                <p>Publicidad Jorge Flores</p>
            </div>
            <nav class="Menu-Navegación">
                <a href="">Inicio</a>
                <a href="">Producto</a>
                <a href="">Servicio</a>
                <a href="">FQA</a>
            </nav>
        </div>
    </div>
    <div class="Encabezado">
        <div class="contenedorlogo">
            <img src="{{ asset('images/jfpublicidadlog.png')}}">
        </div>
        <nav class="contenedornavegacion">
            <a href="{{ asset('home')}}">Inicio</a>
            <a href="{{ asset('productos')}}">Productos</a>
            <a href="{{ asset('proyectos')}}">Proyectos</a>
        </nav>
        <nav class="conetenedorbotones">
            <nav class="contenedornavegacion"> 
                @auth
                <a class="joselo">Bienvenido {{auth()->user()->nombre }}</a>
                @endauth
            </nav>
            @auth
            <div>
                <a class="default-btn" class="default-btn" href="{{ asset('carritocompra')}}"> Mis Compras <span></span></a>
            </div>
            @endauth
            @guest
            <div>
                <a class="default-btn" class="default-btn" href="{{ asset('login')}}"> Mis Compras <span></span></a>
            </div>
            <div>
                @csrf
                <a class="default-btn" href="{{ asset('login')}}">Login <span></span></a>
            </div>
            <div>
                <a class="default-btn" href="{{ asset('registro')}}">Registro <span></span></a>
            </div>
            @endguest
            @auth
            <div>
                <a class="default-btn" href="{{route('login.logout')}}" >Cerrar Sesion<span></span></a>
            </div>
            @endauth
            <div class="control-icono-menu">
                <label for="btn-menulateral"><img src="{{ asset('images/menu.png')}}" class="control-icono-menu"></label>
            </div>
        </nav>
    </div>
    <script type="text/javascript">
        window.addEventListener("scroll", function() {
            var header = document.querySelector("header");
            header.classList.toggle("abajo", window.scrollY > 0);
        })
    </script>
</header>