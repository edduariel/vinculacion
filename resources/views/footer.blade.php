<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('css/PieDePagina.css')}}">
</head>
<footer>
    <!-- Primera Caja-->
    <div class="Conetenedores-Footer">
        <h3> Publicidad J.S <i class="fas fa-shopping-basket"></i> </h3>
        <br>
        <p>Aquí están nuestras redes sociales a las que puedes unirte.</p>
        <div class="contenedor-Icono-Footer">
            <a href="https://www.facebook.com/jorgeflorespublicidad" class="fab fa-facebook-f"></a>
            <a href="#" class="fab fa-twitter"></a>
            <a href="#" class="fab fa-instagram"></a>
            <a href="#" class="fab fa-linkedin"></a>
        </div>
    </div>
    <!-- Segunda Caja-->
    <div class="Conetenedores-Footer">
        <h3>Informacion Contacto</h3>
        <nav class="Footer-nav">
        <br>
            <a href="#" class="links"> <i class="fas fa-phone"></i> 099 952 2444 </a>
            <br>
            <a href="#" class="links"> <i class="fas fa-envelope"></i> pjorgeflores@hotmail.com </a>
            <br>
            <a href="#" class="links"> <i class="fas fa-map-marker-alt"></i> Barrio Jocay calle J-9 entre J-6 y J-4, Manta, Ecuador </a>
        </nav>
    </div>
    <!-- Tercer Caja-->
    <div class="Conetenedores-Footer">
        <h3>Enlaces rápidos</h3>
        <nav class="Footer-nav">
            <br>
            <a href="{{ asset('home')}}" class="links"> <i class="fas fa-arrow-right"></i> Home </a>
            <br>
            <a href="{{ asset('productos')}}" class="links"> <i class="fas fa-arrow-right"></i> Productos </a>
            <br>
            <a href="{{ asset('proyectos')}}" class="links"> <i class="fas fa-arrow-right"></i> Proyectos </a>
            <br>
            <a href="#" class="links"> <i class="fas fa-arrow-right"></i> Servicios </a>
            <br>
            <a href="{{ asset('condiciones')}}" class="links"> <i class="fas fa-arrow-right"></i> Terminos y Condiciones </a>
        </nav>
    </div>
</footer>
</body>