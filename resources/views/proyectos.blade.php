@include ('header')

<body style="background-color:rgba(0,0,0,.1) ;">
    <article class="titulomain">
        <h1> Nuestros <span class="temasec">Proyectos</span> </h1>
    </article>
    <div class="margen">
        <br>
        <br>
        <div class="generalproyect">
            @foreach ($proyectos as $proyecto)
                <form action="{{ route('proyectos.show', $proyecto->id) }} " method="post">

                    <div class="espaciado">
                        <div class="tarjetas">
                            <img src="{{ asset('storage') . '/' . $proyecto->foto }}" class="imgproyecto">
                            <div class="Titulopryecto">
                                <h3 class="tituloproyect">{{ $proyecto->nombre }} </h3>
                            </div>
                            <div class="descripcionproyecto">
                                <div class="textop">
                                    <p class="descripcionp">{{ $proyecto->descripcion }}</p>
                                </div>
                            </div>
                            <button class="verproyecto" type="submit" data-id="{{ $proyecto->id }}">
                                <h4>Leer Más</h4>
                                @csrf
                                @method('post')
                                <div class="">
                                    <input type="hidden" name="id" value="{{ $proyecto->id }}">
                                    <input type="hidden" namespace="_method" value="show">
                                </div>
                            </button>
                        </div>

                    </div>
                </form>
            @endforeach

        </div>
    </div>
 
    @include('footer')
</body>
