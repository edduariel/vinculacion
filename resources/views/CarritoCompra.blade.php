@include ('header')

<head>
    <link rel="stylesheet" href="{{ asset('css/CarritoCompra.css') }}">

</head>


<section class="Carrito">
    @auth
        @php
            $acumulador = 0;
            $contador = 0;
        @endphp
        @foreach ($carritocompras as $carrito)
            @if (auth()->user()->id == $carrito->idCliente && is_null($carrito->numero_orden))
                <div class="Carrito_producto">

                    <figure class="img_container">
                        <img src="{{ asset('storage') . '/' . $carrito->foto }}" class="imagenpepa" alt="...">
                    </figure>
                    <div class="tex_container">
                        @php
                            $total = $carrito->total;
                            $acumulador += $total;
                        @endphp
                        <div>
                            <h2 class="tex_p">{{ $carrito->producto }}</h2>
                            <samp>
                                <h2>{{ $total }}</h2>
                            </samp>
                        </div>

                        <h4 class="tex_d">Disponible</h4>
                        <h4 class="tex_c">{{ $carrito->descripcion }}</h4>
                        <h4 class="tex_c">Detalles:</h4>
                        @if ($carrito->detalles === null)
                            <h4 class="tex_c">No hay detalles</h4>
                        @else
                            <h4 class="tex_c">{{ $carrito->detalles }}</h4>
                        @endif
                        <form action="{{ route('carritocompra.destroy', $carrito->id)}}" method="post" id="formEli_{{$carrito->id}}" >
                        @csrf
                        @method('delete')
                        <input type="hidden" name="id" value="{{$carrito->id}}">
                        <input type="hidden" namespace="_method" value="delete">
                        <button type="submit" class="btn btn-danger" style="color: white;" data-id="{{ $carrito->id}}"> <h4>Eliminar</h4></button>
                        </form>
                    </div>
                   
                </div>
              

            @endif
        @endforeach
        <div class="total">
            <div>
                <h2 class="tex_p">Envio:Gratis</h2>
                <h2 class="tex_p">Total a pagar: {{ $acumulador }}</h2>

            </div>
        </div>
      <div class="total">
        <div class="total2">
            <div class="espaciadototal">
                <div class="izquierda">
                <h2 class="tex_p">Subtotal: </h2>
                </div>
                <div class="derecha">
                <h2>${{ $acumulador }}</h2>
                </div>
            </div>
            <div class="espaciadototal">
                <div class="izquierda">
                <h2 class="tex_p">Envio:</h2>
                </div>
                <div class="derecha">
                <h2>GRATIS</h2>
                </div>
            </div>
            <div class="espaciadototal">
                <div class="izquierda">
                <h1 class="tex_p">Total a pagar: </h1>
                </div>
                <div class="derecha">
                <h1>${{ $acumulador }}</h1>
                </div>
            </div>
        </div>
       
      </div>
    <div class="botonenviar">
    <a href="{{route('compra.indexCompra')}}" type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" >
      <h3>
      Seguir con la Compra
      </h3>  
</a>
    </div>
    @endauth
    @guest
        <div class="Carrito_producto">
            <h1>Inicie Sesion Primero</h1>
        </div>
    @endguest


</section>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>



@include ('footer')
