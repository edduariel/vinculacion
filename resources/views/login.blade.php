@include('header')
<body>
	<div class="contenedor">
		<div class="container">
			<div class="screen">
				<div class="screen__content">
					<form action="{{route('login.store')}}" class="login" method="POST">
						@csrf
						<div class="login__field">
							<i class="login__icon fas fa-user"></i>
							<input name="email" type="email" class="login__input" placeholder="Correo" required>
						</div>
						<div class="login__field">
							<i class="login__icon fas fa-lock"></i>
							<input name="password" type="password" class="login__input" placeholder="Contraseña" required>
						</div>
						<button class="button login__submit">
							<span class="button__text">Iniciar Sesion</span>
						</button>				
					</form>
				</div>
				<div class="screen__background">
					<span class="screen__background__shape screen__background__shape4"></span>
					<span class="screen__background__shape screen__background__shape3"></span>		
					<span class="screen__background__shape screen__background__shape2"></span>
					<span class="screen__background__shape screen__background__shape1"></span>
				</div>		
			</div>
		</div>
	</div>
	@include('footer')
	
</body>
