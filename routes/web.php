<?php

use App\Http\Controllers\LoginClientesController;
use App\Http\Controllers\RegistroController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProyectosController;
use App\Http\Controllers\CarritoCompraController;
use App\Http\Controllers\LoginBackendController;
use App\Http\Controllers\HomeBackendController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProyectosBackController;
use App\Http\Controllers\ComprarProducto;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CondicionesController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\VentasBackController;
use App\Http\Controllers\Ventas2Controller;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/home', [HomeController::class, 'index'])->name('home.index');

Route::get('/productos', [ProductosController::class, 'index']);
Route::post('/productos/ver/{id}', [ProductosController::class, 'show'])->name('producto.show');
Route::GET('/productos/calculo', [ProductosController::class, 'sum'])->name('producto.sum');

Route::post('/productos/ver/{id}/agregarcarrito', [ProductosController::class, 'store'])->name('producto.store');


Route::get('/productos/comprar/1', [ComprarProducto::class, 'index'])->name('comprar.index');
Route::get('/productos/comprar', [ComprarProducto::class, 'show'])->name('comprar.show');

Route::get('/proyectos', [ProyectosController::class, 'index'])->name('proyectos.index');
Route::post('/proyectos/ver/{id}', [ProyectosController::class, 'show'])->name('proyectos.show');

Route::get('/proyectos/vista', [ProyectosController::class, 'indexP'])->name('proyectos.indexP');



Route::get('/condiciones', [CondicionesController::class, 'index'])->name('condiciones.index');


Route::get('/login', [LoginClientesController::class, 'index'])->name('login.index');
Route::get('/login', [LoginClientesController::class, 'logout'])->name('login.logout');
Route::post('/login', [LoginClientesController::class, 'store'])->name('login.store');


//rutas para el registro de la vista cliente
Route::get('/registro', [RegistroController::class, 'index'])->name('registro.index');
Route::post('registro', [RegistroController::class, 'store'])->name('registro.store');
//rutas cliente vista
Route::get('/carritocompra', [CarritoCompraController::class, 'index'])->name('carritocompra.index');
Route::delete('/carritocompra/{id}', [CarritoCompraController::class, 'destroy'])->name('carritocompra.destroy');

Route::get('/compra', [CarritoCompraController::class, 'indexCompra'])->name('compra.indexCompra');
Route::post('/compra', [CarritoCompraController::class, 'updateCompra'])->name('compra.updateCompra');

Route::get('/factura', [CarritoCompraController::class, 'factura'])->name('compra.factura');
Route::get('factura/{id}/{numero_orden}', [Ventas2Controller::class, 'showcliente'])->name('ventas.showcliente');

//rutas para backend
 Route::group(['prefix' => 'backend'], function () {

        Route::get('/loginbackend', [LoginBackendController::class, 'index'])->name('loginbackend.index');
        Route::get('/loginbackend', [LoginBackendController::class, 'logout'])->name('loginbackend.logout');
        Route::post('/loginbackend', [LoginBackendController::class, 'store'])->name('loginbackend.store');


        //Productos
        Route::get('/homebackend', [HomeBackendController::class, 'index'])->middleware('can:homebackend.index')->name('homebackend.index');
        Route::get('/homebackend/productos', [ProductoController::class, 'index'])->middleware('can:homebackend.index')->name('productosbackend.index');
        Route::post('/homebackend/productos', [ProductoController::class, 'store'])->middleware('can:homebackend.index')->name('productosbackend.store');
        Route::delete('/homebackend/productos/{id}', [ProductoController::class, 'destroy'])->middleware('can:homebackend.index')->name('productosbackend.destroy');
        Route::post('/homebackend/productos/{id}/editar', [ProductoController::class, 'edit'])->name('productosbackend.edit');
        Route::post('/homebackend/productos/{id}/actualizado', [ProductoController::class, 'update'])->middleware('can:homebackend.index')->name('productosbackend.update');
        //Usuarios Ventas2Controller
        Route::get('/homebackend/ventas', [Ventas2Controller::class, 'index'])->middleware('can:homebackend.index')->name('ventas.index');
        Route::get('homebackend/ventas/{id}/{numero_orden}', [Ventas2Controller::class, 'show'])->name('ventas.show');
        Route::delete('/homebackend/ventas/{id}', [Ventas2Controller::class, 'destroy'])->middleware('can:homebackend.index')->name('ventas.destroy');

        Route::resource('homebackend/users', UserController::class)->names('backend.users');
        //proyectos
        Route::resource('homebackend/proyectos', ProyectosBackController::class)->middleware('can:homebackend.index')->names('backend.proyectos');

        Route::resource('homebackend/banners', BannerController::class)->middleware('can:homebackend.index')->names('backend.banner');



});